# nom ou hash de l'image de base sur l'aquel s'appuyer
FROM node:16

# ajouter des fichiers/dossiers de l'host vers le conteneur
# il est possible d'utiliser ADD qui peux extraire des archive ou des url
COPY . /todomvc


# défini le dossier de travail
WORKDIR /todomvc

# execute des commandes, il est possible dans un shell
RUN npm install

# executable à lancer avec le conteneur
#ENTRYPOINT node

# Commande et paramètres à executer
CMD [ "npm", "run", "dev" ]
