// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('clear localstorage', () => {
    cy.clearLocalStorage('vue-todomvc')
  })

  it('add todo', () => {
    cy.visit('/')
    cy.get('.new-todo').type('ma tache{enter}').type('ma tache 2{enter}')
    cy.get('li.todo').should('have.length', 2)
    cy.get('.todo-count > strong').contains("2")

  })

  it('add other todo', () => {
    cy.get('.new-todo').type('ma tache 3{enter}').type('ma tache 4{enter}')
    cy.get('.todo-count > strong').contains("4")
  })

  it('check todo', () => {
    cy.get('.todo-list > :nth-child(1) > .view > .toggle').click()
    cy.get('.todo-list > :nth-child(1)').should('have.class', 'completed')


    cy.get('.todo-list > :nth-child(2) > .view > .toggle').click()
    cy.get('.todo-list > :nth-child(2)').should('have.class', 'completed')

    cy.get('.todo-count > strong').contains("2")
  })

  it('uncheck todo', () => {
    cy.get('.todo-list > :nth-child(1) > .view > .toggle').click()
    cy.get('.todo-list > :nth-child(1)').should('not.have.class', 'completed')

    cy.get('.todo-count > strong').contains("3")
  })

  it('delete todo', () => {
    cy.get(' .todo-list >:nth-child(1) > .view > .destroy').invoke('show').click()
    cy.get('.todo-count > strong').contains("2")
    cy.get('li.todo').should('have.length', 3)

  })

  it('check if active filter works', () => {
    cy.get('.filters > :nth-child(2) > a').click()
    cy.get('li.todo').should('have.length', 2)
  })

  it('check if completed filter works', () => {
    cy.get('.filters > :nth-child(3) > a').click()
    cy.get('li.todo').should('have.length', 1)
  })

  it('check if all filter works', () => {
    cy.get('.filters > :nth-child(1) > a').click()
    cy.get('li.todo').should('have.length', 3)
  })

  it('remove completed task', () => {
    cy.get('.clear-completed').click()
    cy.get('li.todo').should('have.length', 2)
    cy.get('.todo-count > strong').contains("2")
  })

  
})
